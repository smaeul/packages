# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=nghttp2
pkgver=1.41.0
pkgrel=0
pkgdesc="Experimental HTTP/2 client, server and proxy"
url="https://nghttp2.org/"
arch="all"
options="!check"  # Requires cunit, which we don't currently ship
license="MIT"
depends=""
makedepends="c-ares-dev libev-dev libxml2-dev openssl-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs"
source="https://github.com/tatsuhiro-t/$pkgname/releases/download/v$pkgver/nghttp2-$pkgver.tar.xz"

# secfixes:
#   1.41.0-r0:
#     - CVE-2020-11080

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--disable-static \
		--without-neverbleed \
		--without-jemalloc \
		--disable-python-bindings \
		--enable-app
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c92e8022ccc876fa311f21bc5bf5af75feff8232efb56a4b2ab198031e974d15b67c16c046188cc76552f75a1b2e7115925d6ce1e42d6f94ae482fe69727466d  nghttp2-1.41.0.tar.xz"
