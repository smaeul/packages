# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksmtp
pkgver=20.04.3
pkgrel=0
pkgdesc="SMTP library for KDE"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcoreaddons-dev ki18n-dev kio-dev kjobwidgets-dev kservice-dev
	solid-dev cyrus-sasl-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksmtp-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="c9794ca8dd93e25c79dd22fab38f71a421da1612658830ffe87cc8e751db9624a3d1924edd52d6fdaa97044e4d1accef5b6807f9461f857bedccc00f4d54f109  ksmtp-20.04.3.tar.xz"
