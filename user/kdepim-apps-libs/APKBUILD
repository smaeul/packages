# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdepim-apps-libs
pkgver=20.04.3
pkgrel=0
pkgdesc="Runtime libraries for KDE PIM applications"
url="https://kontact.kde.org/"
arch="all"
license="LGPL-2.1+ AND GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules akonadi-dev
	akonadi-contacts-dev boost-dev gpgme-dev grantlee-dev grantleetheme-dev
	kauth-dev kcodecs-dev kcompletion-dev kconfig-dev kconfigwidgets-dev
	kcontacts-dev kcoreaddons-dev ki18n-dev kimap-dev kitemmodels-dev
	kitemviews-dev kjobwidgets-dev kio-dev kmime-dev kservice-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev libkleo-dev
	pimcommon-dev prison-dev solid-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdepim-apps-libs-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3858a3ec173545383f71a02ca55662f7fd636d55f187cf0a46b1c2b663e793a298f053768e7ed4d0fec812e1cebdfce62a390d4a912bb2fc363673edeee464e1  kdepim-apps-libs-20.04.3.tar.xz"
