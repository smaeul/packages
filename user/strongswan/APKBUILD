# Contributor: Jesse Young <jlyo@jlyo.org>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Lee Starnes <lee@canned-death.us>
pkgname=strongswan
pkgver=5.8.4
pkgrel=0
pkgdesc="IPsec-based VPN solution focused on security and ease of use, supporting IKEv1/IKEv2 and MOBIKE"
url="https://www.strongswan.org/"
arch="all"
pkgusers="ipsec"
pkggroups="ipsec"
license="GPL-2.0 AND RSA-MD5 AND RSA-PKCS11 AND DES"
depends="iproute2"
depends_dev=""
makedepends="$depends_dev linux-headers python3 sqlite-dev openssl-dev curl-dev
	gmp-dev libcap-dev"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-openrc"
install="$pkgname.pre-install"
source="https://download.strongswan.org/$pkgname-$pkgver.tar.bz2

	strongswan.initd
	charon.initd
	"

# secfixes:
#   5.7.1-r0:
#     - CVE-2018-17540

build() {
	# notes about configuration:
	# - try to keep options in ./configure --help order
	# - apk depends on openssl, so we use that
	# - openssl provides ciphers, randomness, etc
	#   -> disable all redundant in-tree copies

	./configure --prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib \
		--with-ipsecdir=/usr/lib/strongswan \
		--with-capabilities=libcap \
		--with-user=ipsec \
		--with-group=ipsec \
		--enable-curl \
		--disable-ldap \
		--disable-aes \
		--disable-des \
		--disable-rc2 \
		--disable-md5 \
		--disable-sha1 \
		--disable-sha2 \
		--enable-gmp \
		--disable-hmac \
		--disable-mysql \
		--enable-sqlite \
		--enable-eap-sim \
		--enable-eap-sim-file \
		--enable-eap-aka \
		--enable-eap-aka-3gpp2 \
		--enable-eap-simaka-pseudonym \
		--enable-eap-simaka-reauth \
		--enable-eap-identity \
		--enable-eap-md5 \
		--enable-eap-tls \
		--disable-eap-gtc \
		--enable-eap-mschapv2 \
		--enable-eap-radius \
		--enable-xauth-eap \
		--enable-farp \
		--enable-vici \
		--enable-attr-sql \
		--enable-dhcp \
		--enable-openssl \
		--enable-unity \
		--enable-ha \
		--enable-cmd \
		--enable-swanctl \
		--enable-shared \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	install -m755 -D "$srcdir/$pkgname.initd" "$pkgdir/etc/init.d/$pkgname"
	install -m755 -D "$srcdir/charon.initd" "$pkgdir/etc/init.d/charon"
}

sha512sums="15e866b0d6cc4ea94f17856b519d926ae08c15d3b62f675f62685d0722ca8fa26b46afb1ad1c866e9d5f347d77a747f57d0c6d7f6bd57762f37d7798f9e28103  strongswan-5.8.4.tar.bz2
8b61e3ffbb39b837733e602ec329e626dc519bf7308d3d4192b497d18f38176789d23ef5afec51f8463ee1ddaf4d74546b965c03184132e217cbc27017e886c9  strongswan.initd
7182bed917585bce9749b4495ad64d3052d2999dbb505c34d568acd6df7b151232ec10c8efe12f0a07d0555ddfe01aad4e3b767b08f17a55ffcbedc57dc9d934  charon.initd"
