# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kgoldrunner
pkgver=20.04.3
pkgrel=0
pkgdesc="Puzzle game with a gold hunt, dodging enemies, and digging around"
url="https://games.kde.org/game.php?game=kgoldrunner"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev kcoreaddons-dev kconfig-dev kconfigwidgets-dev kcrash-dev
	kdbusaddons-dev kdoctools-dev ki18n-dev kio-dev kxmlgui-dev phonon-dev
	libkdegames-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgoldrunner-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1c759f7e7fe7f4369f7fe1b1812f8e5a1e7cd5c6588f67c6998849e01c013826f517401d541130be8d03ab152b737176889a70f7f6c8e2a13cfe440fa9ffb91e  kgoldrunner-20.04.3.tar.xz"
