# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=tiff
pkgver=4.1.0
pkgrel=0
pkgdesc="Library to read, create, and manipulate TIFF image files"
url="http://www.libtiff.org/"
arch="all"
license="MIT"
depends=""
depends_dev="zlib-dev libjpeg-turbo-dev"
makedepends="libtool autoconf automake $depends_dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-tools"
source="http://download.osgeo.org/libtiff/$pkgname-$pkgver.tar.gz"

# secfixes: libtiff
#   4.0.10-r1:
#     - CVE-2019-6128
#     - CVE-2019-7663
#   4.0.9-r1:
#     - CVE-2017-18013
#   4.0.9-r0:
#     - CVE-2017-16231
#     - CVE-2017-16232
#   4.0.8-r1:
#     - CVE-2017-9936
#     - CVE-2017-10688
#   4.0.7-r2:
#     - CVE-2017-7592
#     - CVE-2017-7593
#     - CVE-2017-7594
#     - CVE-2017-7595
#     - CVE-2017-7596
#     - CVE-2017-7598
#     - CVE-2017-7601
#     - CVE-2017-7602
#   4.0.7-r1:
#     - CVE-2017-5225

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-cxx
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Command-line utility programs for manipulating TIFF files"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="fd541dcb11e3d5afaa1ec2f073c9497099727a52f626b338ef87dc93ca2e23ca5f47634015a4beac616d4e8f05acf7b7cd5797fb218758cc2ad31b390491c5a6  tiff-4.1.0.tar.gz"
