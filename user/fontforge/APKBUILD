# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=fontforge
pkgver=2.0.20170731
pkgrel=1
pkgdesc="Open source font editor"
url="https://fontforge.github.io/en-US/"
arch="all"
license="GPL-3.0+ AND BSD-3-Clause"
depends="desktop-file-utils shared-mime-info"
makedepends="freetype-dev giflib-dev glib-dev libjpeg-turbo-dev libtool
	libuninameslist-dev libpng-dev libx11-dev libxml2-dev pango-dev
	python3-dev tiff-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://github.com/fontforge/fontforge/releases/download/${pkgver#2.0.}/fontforge-dist-${pkgver#2.0.}.tar.xz
	python3.patch
	"

build() {
	PYTHON=python3 ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	# skip test 53; it doesn't work.
	make check TESTSUITEFLAGS="1-52 54"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="26f7a40714460716a24dd0229fdb027f3766bcc48db64b8993436ddcb6277898f9f3b67ad4fc0be515b2b38e01370d1c7d9ee3c6ece1be862b7d8c9882411f11  fontforge-dist-20170731.tar.xz
0ca2f3d10a54e4ccb0737ba8e4e768a75e536f70f8892bfe8c23daa5d542d58a1dd4991a982748dcb2c24ed5ae4cb336278a1c01eb434a0463b8b36133b0bb33  python3.patch"
