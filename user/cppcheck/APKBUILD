# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cppcheck
pkgver=2.1
pkgrel=0
pkgdesc="Tool for static C/C++ code analysis"
url="http://cppcheck.sourceforge.net/"
arch="all"
license="GPL-3.0-only"
depends=""
makedepends="cmake pcre-dev qt5-qtbase-dev qt5-qttools-dev z3-dev"
subpackages="$pkgname-gui"
source="cppcheck-$pkgver.tar.gz::https://github.com/danmar/cppcheck/archive/$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_GUI=ON \
		-DBUILD_TESTS=ON \
		-DHAVE_RULES=ON \
		-DUSE_MATCHCOMPILER=ON \
		-DUSE_Z3=ON \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

gui() {
	pkgdesc="$pkgdesc (Qt UI)"
	depends="cppcheck"
	mkdir -p "$subpkgdir"/usr/share "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/cppcheck-gui "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/*.qm "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/share/applications "$subpkgdir"/usr/share/
	mv "$pkgdir"/usr/share/icons "$subpkgdir"/usr/share/
}

sha512sums="7e1ef473168f9af35db0979f85ddb4d66b91c09051e27bc28ea7453dbe94fbf799515701c737820e0a3bea77da0d8ef171313612f65e954718a80bbcb56d54ed  cppcheck-2.1.tar.gz"
