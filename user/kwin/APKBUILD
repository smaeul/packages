# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=kwin
pkgver=5.18.5
pkgrel=0
pkgdesc="Modern, stylish window manager (requires OpenGL)"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires accelerated X11 desktop
license="GPL-2.0+ AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends="qt5-qtmultimedia"
depends_dev="qt5-qtbase-dev libepoxy-dev libxcb-dev kconfig-dev	kcoreaddons-dev
	kwindowsystem-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev python3
	qt5-qtdeclarative-dev qt5-qtscript-dev qt5-qtsensors-dev eudev-dev
	qt5-qtmultimedia-dev qt5-qtx11extras-dev fontconfig-dev freetype-dev
	libdrm-dev libinput-dev libx11-dev libxi-dev libxkbcommon-dev mesa-dev
	wayland-dev xcb-util-cursor-dev xcb-util-image-dev xcb-util-wm-dev

	breeze breeze-dev kactivities-dev kcompletion-dev kconfigwidgets-dev
	kcmutils-dev kcrash-dev kdeclarative-dev kdecoration-dev kirigami2-dev
	kglobalaccel-dev ki18n-dev kiconthemes-dev kidletime-dev kinit-dev
	kio-dev knewstuff-dev knotifications-dev kpackage-dev kscreenlocker-dev
	kservice-dev ktextwidgets-dev kwayland-dev kwidgetsaddons-dev
	kxmlgui-dev plasma-framework-dev qt5-qtvirtualkeyboard-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/kwin-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_TESTING=OFF \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b703891824ec31e8ae29df27a11499579ab95789a838e893d5904e40aeba27b0ddc04836038d5b4c062d75ceed277d07e67eeb1b4e957ccf414f03786037d562  kwin-5.18.5.tar.xz"
