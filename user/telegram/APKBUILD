# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=telegram
pkgver=2.0.1
pkgrel=0
pkgdesc="Telegram messaging app"
url="https://telegram.org/"
arch="all"
options="!check"  # No test suite.
license="GPL-3.0-only WITH OpenSSL-exception"
depends="qt5-qtimageformats"
makedepends="cmake enchant-dev ffmpeg-dev hunspell-dev libdbusmenu-qt-dev
	lz4-dev minizip-dev openal-soft-dev openssl-dev opus-dev pulseaudio-dev
	python3 qt5-qtbase-dev range-v3 xxhash-dev xz-dev zlib-dev"
subpackages=""
source="https://github.com/telegramdesktop/tdesktop/releases/download/v$pkgver/tdesktop-$pkgver-full.tar.gz
	endian.patch
	"
builddir="$srcdir/tdesktop-$pkgver-full"

build() {
	[ -f "$HOME/telegram_credentials.sh" ] || die "You need to have a Telegram API ID."

	. $HOME/telegram_credentials.sh

	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	# Can't use packaged rlottie, API mismatch.
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DDESKTOP_APP_DISABLE_CRASH_REPORTS=ON \
		-DDESKTOP_APP_USE_GLIBC_WRAPS=OFF \
		-DDESKTOP_APP_USE_PACKAGED_EXPECTED=OFF \
		-DDESKTOP_APP_USE_PACKAGED_GSL=OFF \
		-DDESKTOP_APP_USE_PACKAGED_RLOTTIE=OFF \
		-DDESKTOP_APP_USE_PACKAGED_VARIANT=OFF \
		-DTDESKTOP_API_ID="${TELEGRAM_API_ID}" \
		-DTDESKTOP_API_HASH="${TELEGRAM_API_HASH}" \
		-DTDESKTOP_DISABLE_GTK_INTEGRATION=ON \
		-DTDESKTOP_LAUNCHER_BASENAME=telegramdesktop \
		-DTDESKTOP_USE_PACKAGED_TGVOIP=OFF \
		-Ddisable_autoupdate=1 \
		${CMAKE_CROSSOPTS} \
		.
	make
}

package() {
	install -D -m755 "$builddir"/bin/telegram-desktop "$pkgdir"/usr/bin/telegram-desktop
	install -D -m644 "$builddir"/lib/xdg/telegramdesktop.desktop "$pkgdir"/usr/share/applications/telegramdesktop.desktop
	for _icon in 16 32 48 64 128 256 512; do
		install -D -m644 "$builddir"/Telegram/Resources/art/icon$_icon.png \
			"$pkgdir"/usr/share/icons/hicolor/${_icon}x${_icon}/apps/telegram.png
	done
}

sha512sums="99cd7c5ca1e9dd75ecd98d272522b0e4aab2d46525e3d0c306503b7a00c9d25c1646e9d7462182682a58947c7435864af805a3b6f85906d8b21e5675cc8383cb  tdesktop-2.0.1-full.tar.gz
c478bd59187493d60172d805ca19e9e09fa2c81b87d5dbbd5f3cd9aae0f207b463d127e06f2053f7b7b6ac00b3191d59e36ec6c5453a1da4d6535d1caad27242  endian.patch"
