# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=cantor
pkgver=20.04.3
pkgrel=0
pkgdesc="KDE worksheet interface for popular mathematical applications"
url="https://edu.kde.org/cantor/"
arch="all"
options="!check"  # Tests require X11.
license="GPL-2.0-only"
depends="r"
depends_dev="libspectre-dev"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev karchive-dev kcompletion-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev kiconthemes-dev
	kio-dev knewstuff-dev kparts-dev kpty-dev ktexteditor-dev
	ktextwidgets-dev kxmlgui-dev $depends_dev poppler-dev poppler-qt5-dev
	
	analitza-dev gfortran libqalculate-dev python3-dev r r-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/cantor-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="07351d8b4b72653394a337bc3ae5c8f8c669dd8cd66f4cf9d41bbdd25b47a8b3666f79580e319ef56c878ab626ce959d825b4a34243c5f639de96674a1bc111d  cantor-20.04.3.tar.xz"
