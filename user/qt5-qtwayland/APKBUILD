# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=qt5-qtwayland
_pkgname=qtwayland-everywhere-src
pkgver=5.12.9
pkgrel=0
pkgdesc="Unstable Qt platform module for experimental Wayland display system"
url="https://www.qt.io/"
arch="all"
options="!check"
license="LGPL-3.0-only WITH Qt-LGPL-exception-1.1 OR GPL-3.0-only WITH Qt-GPL-exception-1.0"
depends=""
makedepends="libxkbcommon-dev mesa-dev qt5-qtbase-dev wayland-dev"
subpackages="$pkgname-dev $pkgname-client $pkgname-compositor $pkgname-tools"
source="https://download.qt.io/official_releases/qt/${pkgver%.*}/$pkgver/submodules/$_pkgname-$pkgver.tar.xz"
builddir="$srcdir"/$_pkgname-$pkgver

build() {
	qmake
	make
}

package() {
	make install INSTALL_ROOT="$pkgdir"
}

client() {
	pkgdesc="Qt client library for experimental Wayland display system"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libQt5WaylandClient* "$subpkgdir"/usr/lib/
}

compositor() {
	pkgdesc="Unstable Qt compositor library for experimental Wayland display system"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libQt5WaylandCompositor* "$subpkgdir"/usr/lib/
}

tools() {
	pkgdesc="Tools for Qt integration with experimental Wayland display system"
	mkdir -p "$subpkgdir"/usr/lib/qt5
	mv "$pkgdir"/usr/lib/qt5/bin "$subpkgdir"/usr/lib/qt5/
}

sha512sums="9a1b37736cc87da09123ce3b21709ea34d1171c35c27636b90c5c18e95bfa8a1ac2807c6b54e7672d967cd81cff90461f56de86aad8d5fa647434016f5fb7e12  qtwayland-everywhere-src-5.12.9.tar.xz"
