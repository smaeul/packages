# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=lynx
pkgver=2.8.9_p1
_relver="${pkgver%_p*}rel.${pkgver#*_p}"
pkgrel=0
pkgdesc="Cross-platform text-based browser"
url="https://lynx.invisible-island.net/"
arch="all"
license="GPL-2.0-only"
depends="gzip"
makedepends="glib-dev ncurses-dev openssl-dev perl utmps-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://invisible-mirror.net/archives/lynx/tarballs/${pkgname}${_relver}.tar.bz2"
builddir="$srcdir/${pkgname}${_relver}"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-ipv6 \
		--enable-nls \
		--with-ssl
	make helpdir=/usr/share/doc/lynx/help \
		docdir=/usr/share/doc/lynx
}

check() {
	./lynx -version
}

package() {
	make DESTDIR="$pkgdir" install install-help install-doc \
		helpdir=/usr/share/doc/lynx/help \
		docdir=/usr/share/doc/lynx
}

sha512sums="61edbe082684fcbd91bdbf4f4d27c3baf92358811aaffc2f8af46adf23ca7b48aede1520fc5f2a8fc974a2f4bbf4e57e7e6027a187bfc6101e56878c98178e6d  lynx2.8.9rel.1.tar.bz2"
