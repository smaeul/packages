# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kamoso
pkgver=20.04.3
pkgrel=0
pkgdesc="KDE camera software"
url="https://kde.org/applications/multimedia/org.kde.kamoso"
arch="all"
license="GPL-2.0+"
depends="gst-plugins-base qt5-qtgraphicaleffects qt5-qtquickcontrols
	qt5-qtquickcontrols2"
makedepends="qt5-qtbase-dev qt5-qtdeclarative-dev cmake extra-cmake-modules
	kauth-dev kcodecs-dev kcompletion-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kdoctools-dev ki18n-dev kio-dev kitemviews-dev
	kjobwidgets-dev knotifications-dev kservice-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev purpose-dev solid-dev gstreamer-dev
	gst-plugins-base-dev kirigami2-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kamoso-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	QT_QPA_PLATFORM=offscreen CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="7da5f635cd5744215e7d3c3332729c149786b10a50ecb6c1e63843da8756da2dd2b355b837d24bed670c52990c7f5322b52d703ca61c5c6ec87d89de04458e80  kamoso-20.04.3.tar.xz"
