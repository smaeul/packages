# Contributor: Kiyoshi Aman <adelie@aerdan.vulpine.house>
# Maintainer: Kiyoshi Aman <adelie@aerdan.vulpine.house>
pkgname=irssi
pkgver=1.2.2
pkgrel=0
pkgdesc="Text-based IRC client"
url="https://irssi.org"
arch="all"
license="GPL-2.0+ AND ISC"
depends=""
makedepends="ncurses-dev glib-dev openssl-dev perl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-perl"
source="https://github.com/irssi/irssi/releases/download/$pkgver/irssi-$pkgver.tar.xz"

# secfixes: irssi
#   1.2.1-r0:
#     - CVE-2019-13045
#   1.2.2-r0:
#     - CVE-2019-15717

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-true-color \
		--with-perl=module \
		--with-perl-lib=vendor
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

perl() {
	depends="perl"
	pkgdesc="Perl support & scripts for irssi"

	mkdir -p "$subpkgdir"/usr "$subpkgdir"/usr/share/irssi
	mv "$pkgdir"/usr/lib "$subpkgdir"/usr
	mv "$pkgdir"/usr/share/irssi/scripts "$subpkgdir"/usr/share/irssi
}
sha512sums="5444ac102ff9ad3a6399a47c967d138e181330dd226eac68886d35fee4ad455932b9306a367bee3478095158e41ba67fb46deb8f0a33512046b9b83bae37c610  irssi-1.2.2.tar.xz"
