# Contributor: k0r10n <k0r10n.dev@gmail.com>
# Contributor: Jose-Luis Rivas <ghostbar@riseup.net>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=libzip
pkgver=1.6.1
pkgrel=0
pkgdesc="C library for manipulating ZIP archives"
url="https://libzip.org/"
arch="all"
license="BSD-3-Clause"
depends=""
depends_dev="zlib-dev"
makedepends="$depends_dev cmake groff openssl-dev perl"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="https://libzip.org/download/$pkgname-$pkgver.tar.xz"

# secfixes:
#   1.3.0:
#     - CVE-2017-14107

build() {
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" .
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" -j1 install
}

tools() {
	pkgdesc="$pkgname cmp and merge tools"
	install -d "$subpkgdir"/usr
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr
}

sha512sums="dcf3790933c95f66bdcbdb276497b36e57776103b0b1064a94479e84eaa0a09df8dd91509cb4ccab3a6724f0650f076ca1e332d73acc94b653e99a3e94a64574  libzip-1.6.1.tar.xz"
