# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=thunderbird
pkgver=60.2.1
pkgrel=0
pkgdesc="Email client from Mozilla"
url="https://www.mozilla.org/thunderbird/"
arch="all"
options="!check"  # X11 required
license="MPL"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 ncurses-dev perl cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	cargo clang fts-dev llvm6-dev rust
	dbus-glib-dev gconf-dev gtk+2.0-dev gtk+3.0-dev hunspell-dev libsm-dev
	libnotify-dev libxcomposite-dev libxdamage-dev libxrender-dev libxt-dev
	nss-static sqlite-dev startup-notification-dev unzip yasm zip
	"
subpackages=""
source="https://archive.mozilla.org/pub/thunderbird/releases/$pkgver/source/thunderbird-$pkgver.source.tar.xz
	mozconfig
	bad-google-code.patch
	cmsghdr.patch
	endianness-js.patch
	fix-bug-1261392.patch
	fix-seccomp-bpf.patch
	proper-system-hunspell.patch
	rust-config.patch
	skia.patch
	stab.h
	stackwalk-x86-ppc.patch
	thunderbird.desktop
	webrtc-broken.patch
	"
somask="liblgpllibs.so
	libmozgtk.so
	libmozsandbox.so
	libxul.so"
_tbirddir=/usr/lib/${pkgname}

unpack() {
	default_unpack
	# just ripped from Firefox's APKBUILD...
	[ -z $SKIP_PYTHON ] || return 0
	msg "Killing all remaining hope for humanity and building Python 2..."
	cd "$srcdir"
	[ -d python ] && rm -r python
	mkdir python
	cd python
	# 19:39 <+solar> just make the firefox build process build its own py2 copy
	curl -O https://www.python.org/ftp/python/2.7.15/Python-2.7.15.tar.xz
	tar xJf Python-2.7.15.tar.xz
	cd Python-2.7.15
	# 20:03 <calvin> TheWilfox: there's always violence
	./configure --prefix="$srcdir/python"
	make -j $JOBS
	# 6 tests failed:
	#    test__locale test_os test_posix test_re test_strptime test_time
	# make test
	make -j $JOBS install
}

prepare() {
	default_prepare
	cp "$srcdir"/stab.h "$builddir"/toolkit/crashreporter/google-breakpad/src/
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	# too much memory
	if [ -z "$JOBS" ] || [ $JOBS -gt 32 ]; then
		echo "mk_add_options MOZ_MAKE_FLAGS=\"-j32\"" >> "$builddir"/mozconfig
	fi
}

build() {
	cd "$builddir"

	# reportedly needed for gcc6; confirm this?
	export CXXFLAGS="$CXXFLAGS -fno-delete-null-pointer-checks -fno-schedule-insns2"

	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_tbirddir}"
	export USE_SHORT_LIBNAME=1

	export PATH="$srcdir/python/bin:$PATH"
	./mach build
}

package() {
	cd "$builddir"
	export PATH="$srcdir/python/bin:$PATH"
	DESTDIR="$pkgdir" ./mach install
	install -D -m644 "$srcdir"/thunderbird.desktop \
		"$pkgdir"/usr/share/applications/thunderbird.desktop
}

sha512sums="d74da3c90658c5baf09c22760cad31594524c09f2cd5aba81c5b15bb6db64d78f613562cb015d8a725b4902caa4a57a2d1dafce28284533747faed00f8268a02  thunderbird-60.2.1.source.tar.xz
beda68f67171d18291c0671a5c7944382c217f47a1abceeb9e776fe7cf2d5baf861616bc1450032964ce78f910765c8c6b497eb1b050c85ac65a560bae5c9c87  mozconfig
dce2523e9dc10719eae77cc1607de094e34526b3ff8694fb51a5116c118c89bb1c1b8b192c03b976663b3afab8e9fdbff79007b3ba0344186a7783b3dc597ebc  bad-google-code.patch
61c7117345b4bcb045ce0287aa368d90dffd7331347021ff345d5a7c46097ffd2575f38711309ce7bdbbbec15561e03fdabcb16235ed8a325ccc8c9d5bba35a6  cmsghdr.patch
31f0cf9663443410a996002ed0357c2ea21692ce965a30d8bfb65d0f1827173da8d8b5c831ccbc199ecbe7a577613be0dd54a09db08e620fe37e1ce977a7afb0  endianness-js.patch
a50b412edf9573a0bd04a43578b1c927967a616b73a5995eefb15bfa78fd2bd14e36ec05315a0703f6370ecd524e6bcb012e7285beb1245e9add9b8553acb79e  fix-bug-1261392.patch
70863b985427b9653ce5e28d6064f078fb6d4ccf43dd1b68e72f97f44868fc0ce063161c39a4e77a0a1a207b7365d5dc7a7ca5e68c726825eba814f2b93e2f5d  fix-seccomp-bpf.patch
1a88c21c39d5878e2018463fe08dd3a960cdf10e650e06ef3e4f089a431800b50267d8749a7acde196bb47c45aa66058a6ed4fb4d60de2ab69e8a48cd5a109bc  proper-system-hunspell.patch
fd3b165e26ab931dd7eaf220d578c30b8772eab0a870710d59403c9823c2233ef941cd7eb25d1625d705de9e8a7138d0e8c4e227a185e9b687553132da96d81a  rust-config.patch
8ef2bc4404601f66c7505f3e0a2f6d1d11e8f11e5a888dce4942cf27c86fbdbcdd66cb3d98738b7d9e25538793986140e75d51a893d22c70714ed98ef50a9894  skia.patch
0b3f1e4b9fdc868e4738b5c81fd6c6128ce8885b260affcb9a65ff9d164d7232626ce1291aaea70132b3e3124f5e13fef4d39326b8e7173e362a823722a85127  stab.h
d12ce112b97cbdba0748c5734a024b13032c5e0696efbd499764246e031d477b0f0a966aedc7b3bedd166bcdc2cc24d45bb9da5f678de9cff547bc6aa231cd16  stackwalk-x86-ppc.patch
95a2b1deb4f6c90750fdd2bfe8ca0a7879a5b267965091705a6beb0a0a4b1ccad75d11df7b9885543ca4232ff704e975c6946f4c11804cb71c471e06f9576001  thunderbird.desktop
76409619464259e3ba52e38d640c5b6210a7fecdc75cf124c185ef356507d4d266a845e9fcdeb7766dcd547f70748123c4fa1670f6e52aadd001a3c866dc2d51  webrtc-broken.patch"
