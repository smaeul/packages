# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=pkgconf
pkgver=1.7.3
pkgrel=0
pkgdesc="Toolkit for maintaining development package metadata"
url="http://pkgconf.org/"
arch="all"
license="ISC"
depends=""
checkdepends="kyua atf"
makedepends=""
provides="pkgconfig=1"
subpackages="$pkgname-doc $pkgname-dev"
source="https://distfiles.dereferenced.org/pkgconf/pkgconf-$pkgver.tar.xz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-pkg-config-dir=/usr/local/lib/pkgconfig:/usr/local/share/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	ln -s pkgconf "$pkgdir"/usr/bin/pkg-config
}

dev() {
	default_dev

	# Move pkg-config back to main package (default_dev implicitly moves
	# files /usr/bin/*-config to -dev).
	mv "$subpkgdir"/usr/bin/pkg-config "$pkgdir"/usr/bin/

	mkdir -p "$pkgdir"/usr/share/aclocal/
	mv "$subpkgdir"/usr/share/aclocal/pkg.m4 "$pkgdir"/usr/share/aclocal/
}

sha512sums="37b6c4f9f3b93970e35b6970fde22fbbde65e7fa32a5634b3fdfc25cc1f33843582722ad13d9a8e96fd6768406fcbe86bf5feb76996ddd0bb66d6ff91e65f0b6  pkgconf-1.7.3.tar.xz"
