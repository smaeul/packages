# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=acl
pkgver=2.2.53
pkgrel=0
pkgdesc="Access control list utilities"
url="https://savannah.nongnu.org/projects/acl"
arch="all"
# depends on order of nftw, and requires test machine to have ACLs enabled on FS
options="!check"
license="LGPL-2.1+ AND GPL-2.0+"
depends=""
makedepends_build="gzip gettext-tiny"
makedepends_host="attr-dev gettext-tiny-dev"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang libacl"
source="https://download.savannah.nongnu.org/releases/acl/acl-$pkgver.tar.gz"

prepare() {
	default_prepare
	sed -i \
		-e '/^as_dummy=/s:=":="$PATH$PATH_SEPARATOR:' \
		configure # hack PATH with AC_PATH_PROG
}

build() {
	CONFIG_SHELL=/bin/sh ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--libdir=/lib \
		--libexecdir=/usr/lib
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

libacl() {
	pkgdesc="Dynamic library for access control list support"
	mkdir -p "$subpkgdir"/lib
	mv "$pkgdir"/lib/libacl.so.* "$subpkgdir"/lib/
}

sha512sums="176b7957fe0e7618e0b7bf2ac5071f7fa29417df718cce977661a576fa184e4af9d303b591c9d556b6ba8923e799457343afa401f5a9f7ecd9022185a4e06716  acl-2.2.53.tar.gz"
