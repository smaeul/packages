# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=expect
pkgver=5.45.4
pkgrel=0
pkgdesc="A tool for automating interactive applications"
url="https://www.nist.gov/services-resources/software/expect"
arch="all"
license="Public-Domain"
makedepends="tcl-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://downloads.sourceforge.net/project/expect/Expect/$pkgver/$pkgname$pkgver.tar.gz"
builddir="$srcdir"/$pkgname$pkgver

prepare() {
	default_prepare
	update_config_sub
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--mandir=/usr/share/man \
		--with-tcl=/usr/lib \
		--with-tclinclude=/usr/include \
		--disable-rpath
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a8dc25e8175f67e029e15cbcfca1705165c1c4cb2dd37eaaaebffb61e3ba132d9519cd73ca5add4c3358a2b0b7a91e878279e8d0b72143ff2c287fce07e4659a  expect5.45.4.tar.gz"
